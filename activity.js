//#2
db.users.find({$or: [
	{"lastName": {$regex: "s", $options: "$i"}}, 
	{
		"firstName": 1,
		"lastName": 1,
		"_id": 0

	}]

});

//# 3
db.users.find({$and: [{"department": "HR"},
	{"age": {$gte:70}}
	]});

//#4
db.users.find({$and: [
	{"firstName": {$regex: "e", $options: "$i"}}, 
	{"age": {$lte:30}
	}]

});